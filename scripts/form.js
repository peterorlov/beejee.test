window.addEventListener('DOMContentLoaded', function () {
    'use strict';

    let AddNewTaskForm = document.querySelector('#AddNewTAskForm'),
        AddNewTaskFormButton = document.querySelector('#AddNewTAskForm button'),
        exampleInputUserName = document.querySelector('#exampleInputUserName'),
        exampleInputUserEmail = document.querySelector('#exampleInputUserEmail'),
        exampleInputUserTask = document.querySelector('#exampleInputUserTask'),
        UserName = document.querySelector('#UserName'),
        UserEmail = document.querySelector('#UserEmail'),
        UserTask = document.querySelector('#UserTask');


    exampleInputUserName.addEventListener('change', function () {
        checkField(/[A-Za-zА-Яа-я]{1,100}/g, exampleInputUserName, UserName, 'Must be only alphabetic characters', 'This is cool!');
    });

    exampleInputUserEmail.addEventListener('change', function () {
        checkField(/([A-Za-z0-9]{1,30})@([A-Za-z0-9]{1,30})\.([A-Za-z0-9]{1,30})/g, exampleInputUserEmail, UserEmail, 'Email must contain only alphabetic and numeric characters', 'This is cool!');
    });

    exampleInputUserTask.addEventListener('change', function () {
        checkField(/.*/, exampleInputUserTask, UserTask, 'Cannot be empty', 'This is cool!');
    });

    AddNewTaskForm.addEventListener('change', function (event) {
        event.preventDefault();

        if (+UserName.dataset.success && +UserEmail.dataset.success && +UserTask.dataset.success) {
            AddNewTaskFormButton.removeAttribute('disabled');
        } else {
            AddNewTaskFormButton.setAttribute('disabled', 'disabled');
        }
    });
});

function invalidField(field) {
    field.classList.remove('is-valid');
    field.classList.add('is-invalid');
}

function validField(field) {
    field.classList.remove('is-invalid');
    field.classList.add('is-valid');
}

function formDangerAlert(field) {
    field.classList.remove('text-success');
    field.classList.add('text-danger');
}

function formSuccessAlert(field) {
    field.classList.remove('text-danger');
    field.classList.add('text-success');
}

function setValidationAttribute(field, validation = false) {
    if (validation) {
        field.dataset.success = '1';
    } else {
        field.dataset.success = '0';
    }
}

function checkField(regExp, elem, textField, dangerText, successText) {
    const reg = regExp,
        result = reg.exec(elem.value);

    reg.lastIndex = 0;
    if ((result && (result[0] !== elem.value)) || !result) {
        invalidField(elem);
        formDangerAlert(textField);
        setValidationAttribute(textField);
        textField.innerText = dangerText;
    } else {
        validField(elem);
        formSuccessAlert(textField);
        setValidationAttribute(textField, true);
        textField.innerText = successText;
    }
}