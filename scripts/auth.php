<?php

function auth() {
    if ($_POST['admin_login']) {
        $admin_name = htmlspecialchars(trim($_POST['admin_name']));
        $admin_pass = htmlspecialchars(trim($_POST['admin_pass']));
        $admin_hash = password_hash('123', PASSWORD_DEFAULT);
        if(!password_verify($admin_pass, $admin_hash)) {
            setcookie('password_check', 0);
        } elseif(password_verify($admin_pass, $admin_hash)) {
            setcookie('password_check', 1);
        }
        if($admin_name !== 'admin') {
            setcookie('login_check', 0);
        } elseif($admin_name === 'admin') {
            setcookie('login_check', 1);
        }

        if ($admin_name === 'admin' && password_verify($admin_pass, $admin_hash)) {
            $_SESSION['admin_name'] = 'admin';
            header('Location: /admin');
        }
    }
}