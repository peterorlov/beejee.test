<?php

$array = explode('?', $_SERVER['REQUEST_URI']);
if ($array[0] === '/' || $array[0] === '/index.php') {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/content/index.php";
} else {
    $page = str_replace('/', '', $array[0]);
    if (!isset($_SESSION['admin_name'])) {
        $page = 'login';
    }
    include_once $_SERVER['DOCUMENT_ROOT'] . "/content/" . $page . ".php";

}