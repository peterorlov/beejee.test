window.addEventListener('DOMContentLoaded', function () {
    'use strict';

    const warningMessage = document.querySelector('#WarningMessage'),
        valuesArray = getCookies();

    if (valuesArray) {
        if (!+valuesArray[0] || !+valuesArray[1]) {
            warningMessage.innerText = "The login/password pair is incorrect or one of the fields is empty";
        }
    }
});

function getCookies() {
    if (document.cookie && document.cookie.indexOf('login_check') + 1 && document.cookie.indexOf('password_check') + 1) {
        let data = document.cookie,
            array = data.split('; '),
            values = new Array(array.length);

        for (let i = 0; i < 2; i++) {
            const splitElem = array[i].split('=');
            if ((splitElem[0].indexOf('login_check') + 1) || (splitElem[0].indexOf('password_check') + 1)) {
                values[i] = splitElem[1];
            }
        }
        return values;
    }
}