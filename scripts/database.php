<?php

//$link = mysqli_connect("localhost", "root", "", "beejee");
$link = mysqli_connect("localhost", "genesis", "3edCVBgt5", "g3n3515");

if ($_POST['submit']) {
    new_task($link);
}

function get_tasks($link)
{
    if (!$_GET['sort'])
        $sort = 'user_name';
    else
        $sort = $_GET['sort'];

    $query = "SELECT * FROM tasks ORDER BY " . $sort;

    $response = mysqli_fetch_all(mysqli_query($link, $query));
    return $response;
}

function new_task($link)
{
    $user_name = htmlspecialchars(trim($_POST['user_name']));
    $user_email = htmlspecialchars(trim($_POST['user_email']));
    $user_task = htmlspecialchars(trim($_POST['user_task']));
    $status = 0;
    $edited = 0;

    $query = "INSERT INTO tasks (user_name, user_email, user_task, status, edited) VALUES ('{$user_name}', '{$user_email}', '{$user_task}', {$status}, {$edited})";

    $result = mysqli_query($link, $query);

    setcookie('new_task_result', $result, time() + 20);
    header('location: /');
}

function add_task_result()
{
    if (isset($_COOKIE['new_task_result']) && $_COOKIE['new_task_result']) {
        $response = "Task added successfully";
    } else if (isset($_COOKIE['new_task_result']) && !$_COOKIE['new_task_result']) {
        $response = "Error in adding task";
    }

    return $response;
}

function statusInProgress($value) {
    if($value === "In progress")
        return "Selected";
}

function statusComplete($value) {
    if($value === "Complete")
        return "Selected";
}

function edit_task($link) {
    if (isset($_SESSION['admin_name'])) {
        $input_data = $_POST;
        array_pop($input_data);

        foreach ($input_data as $key => $value) {
            if(stristr($key, 'id_')) {
                $new_key = str_replace('id_', '', $key);
                $middle_data[$new_key]['id'] = $value;
            }
            if(stristr($key, 'user_task_')) {
                $new_key = str_replace('user_task_', '', $key);
                $middle_data[$new_key]['user_task'] = $value;
            }
            if(stristr($key, 'status_')) {
                $new_key = str_replace('status_', '', $key);
                $middle_data[$new_key]['status'] = $value;
            }
        }

        $query = "SELECT * FROM tasks";
        $response = mysqli_fetch_all(mysqli_query($link, $query));

        for ($i = 0; $i < count($middle_data); $i++) {
            for ($j = 0; $j < count($response); $j++) {
                if ($middle_data[$i]['id'] === $response[$j][0]) {
                    if (($middle_data[$i]['user_task'] !== $response[$j][3]) || ($middle_data[$i]['status']) !== $response[$j][4]) {
                        $new_request[$i] = $middle_data[$i];
                    }
                }
            }
        }

        sort($new_request);

        for ($i = 0; $i < count($new_request); $i++) {
            $id = $new_request[$i]['id'];
            $user_task = htmlspecialchars(trim($new_request[$i]['user_task']));
            $status = $new_request[$i]['status'];
            $edited = "Edited by Admin";

            $query = "UPDATE tasks SET user_task = '{$user_task}', status = '{$status}', edited = '{$edited}', updated_at = now() WHERE id = {$id}";
            $result = mysqli_query($link, $query);

            if ($result) {
                header('Location: /admin');
            }
        }
    }
}