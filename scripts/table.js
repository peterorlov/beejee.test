window.addEventListener('DOMContentLoaded', function () {
    'use strict';

    const tableElements = document.querySelectorAll('.task_row'),
        sortButtons = document.querySelectorAll('.sort_button');

    let j = 1;
    for (let i = 0; i < tableElements.length; i++) {
        if ((i !== 0) && (i % 3 === 0)) {
            j++;
        }
        tableElements[i].dataset.tablepage = j;
    }

    for (let i = 0; i < j; i++) {
        const newElem = document.createElement('button');
        newElem.classList.add('btn');
        newElem.classList.add('btn-link');
        newElem.id = `table_page_${i + 1}`;
        newElem.dataset.page = i + 1;
        newElem.innerText = i + 1;
        document.querySelector('#pages_selector').insertAdjacentElement('beforeend', newElem);
    }

    for (let i = 0; i < tableElements.length; i++) {
        if (tableElements[i].dataset.tablepage !== '1') {
            tableElements[i].classList.add('d-none');
        }
    }

    const pagesSelectorButtons = document.querySelectorAll('#pages_selector button');

    for (let i = 0; i < pagesSelectorButtons.length; i++) {
        pagesSelectorButtons[i].addEventListener('click', function () {
            for (let j = 0; j < tableElements.length; j++) {
                if (tableElements[j].dataset.tablepage === pagesSelectorButtons[i].dataset.page) {
                    tableElements[j].classList.remove('d-none');
                    tableElements[j].classList.add('d-table-row');
                } else {
                    tableElements[j].classList.remove('d-table-row');
                    tableElements[j].classList.add('d-none');
                }
            }
        })
    }

    addStatusStyle();
    newTaskAddedAlert();
    sortButtonStyle(sortButtons);
});

function addStatusStyle() {
    const tasksStatuses = document.querySelectorAll('.task_status');

    tasksStatuses.forEach(function (item) {
        if (item.innerHTML === 'Complete') {
            item.parentNode.classList.add('table-success');
        }
    });
}

function newTaskAddedAlert() {
    const alertMessage = document.querySelector('#information');

    if (alertMessage.textContent === 'Task added successfully') {
        alertMessage.classList.remove('bg-warning');
        alertMessage.classList.add('bg-success');
    } else {
        alertMessage.classList.remove('bg-success');
        alertMessage.classList.add('bg-warning');
    }
}

function sortButtonName() {
    const uri = window.location.search,
        array = uri.split('&');

    let str = '';

    for (let i = 0; i < array.length; i++) {
        if (array[i].indexOf('sort') + 1) {
            str = array[i].replace('?', '');
            str = str.replace('sort=', '');
        }
    }

    return str;
}

function sortButtonStyle(sortButtons) {
    let activeButton = sortButtonName();
    if (activeButton === '') {
        activeButton = 'user_name';
    }
    activeButton += '_button';

    for (let i = 0; i < sortButtons.length; i++) {
        if (sortButtons[i].id === activeButton) {
            sortButtons[i].classList.remove('btn-light');
            sortButtons[i].classList.add('btn-success');
        } else {
            sortButtons[i].classList.remove('btn-success');
            sortButtons[i].classList.add('btn-light');
        }
    }
}