<form action="" method="post" class="row bg-light p-4" id="AddNewTAskForm">
    <div class="col-12">
        <h4 class="text-center">Add new Task</h4>
    </div>
    <div class="form-group col-6">
        <label for="exampleInputUserName">Username</label>
        <input type="text" class="form-control" id="exampleInputUserName" aria-describedby="UserName"
               placeholder="Enter your Name" name="user_name">
        <small id="UserName" class="form-text"></small>
    </div>
    <div class="form-group col-6">
        <label for="exampleInputUserEmail">Email</label>
        <input type="email" class="form-control" id="exampleInputUserEmail" aria-describedby="UserEmail"
               placeholder="Enter your Email" name="user_email">
        <small id="UserEmail" class="form-text"></small>
    </div>
    <div class="form-group col-12">
        <label for="exampleInputUserTask">Task</label>
        <textarea class="form-control" id="exampleInputUserTask" aria-describedby="UserTask"
                  placeholder="Enter your Task" name="user_task"></textarea>
        <small id="UserTask" class="form-text"></small>
    </div>
    <button class="btn btn-secondary" type="submit" name="submit" value="submit" disabled>Submit Task</button>
</form>