<table class="table table-hover shadow" id="tasks_table">
    <thead class="thead-dark">
    <tr>
        <th scope="col" class="col-3">User Name</th>
        <th scope="col" class="col-2">User Email</th>
        <th scope="col" class="col-5">Task</th>
        <th scope="col" class="col-2">Status</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $tasks_table = get_tasks($link);

    foreach ($tasks_table as $value) {
        echo "<tr class='task_row' scope='row'>";
        echo "<td>" . $value[1] . "</td>";
        echo "<td>" . $value[2] . "</td>";
        echo "<td>" . $value[3] . "</td>";
        echo "<td class='task_status'>" . $value[4] . "</td>";
        echo "</tr>";
    }

    ?>
    </tbody>
</table>
