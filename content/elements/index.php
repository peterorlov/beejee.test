<body class="container p-5">
<h3 class="text-center">BeeJee Test</h3>
<div class="row justify-content-between align-items-center mb-2">
    <div class="col-4">
        <h1 class="text-muted">Tasks list</h1>
    </div>
    <div class="col-2">
        <a href="/login" class="btn btn-primary align-self-end">To admin panel</a>
    </div>
</div>
<div id="information" class="col-12 my-2"><?php echo $form_result; ?></div>
<div class="row align-items-center mb-2">
    <div class="col-12">
        <span>Sort by:</span>
        <a href="?sort=user_name" class="btn btn-success mx-1 sort_button" id="user_name_button">Name</a>
        <a href="?sort=user_email" class="btn btn-info mx-1 sort_button" id="user_email_button">Email</a>
        <a href="?sort=status" class="btn btn-info mx-1 sort_button" id="status_button">Status</a>
    </div>
</div>


<!-- Список задач -->
<?php include_once $_SERVER['DOCUMENT_ROOT'] . "/content/elements/task_list.php"; ?>

<div id="pages_selector" class="row justify-content-center align-items-center">
    <span class="text-primary">Pages:</span>
</div>

<script defer src="../scripts/table.js"></script>
<script defer src="../scripts/form.js"></script>
