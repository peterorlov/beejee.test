<form action="" method="post" id="admin_table">
    <?php include_once $_SERVER['DOCUMENT_ROOT'] . "/scripts/database.php"; ?>

    <table class="table table-sm table-hover shadow" id="tasks_table">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="col-2">User Name</th>
            <th scope="col" class="col-2">User Email</th>
            <th scope="col" class="col-4">Task</th>
            <th scope="col" class="col-2">Status</th>
            <th scope="col" class="col-2">Edited by Admin</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $tasks_table = get_tasks($link);

        foreach ($tasks_table as $key => $value) {
                echo
                "<tr class='task_row' scope='row'>
                    <input type='hidden' name='id_{$key}' value='{$value[0]}'>
                    <td class='form-group col-2'>{$value[1]}</td>
                    <td class='form-group col-2'>{$value[2]}</td>
                    <td class='form-group col-4'>
                        <input type='text' name='user_task_{$key}' class='form-control form-control-sm' value='{$value[3]}' placeholder='Enter User Name' form='admin_table'>
                    </td>
                    <td class='form-group col-2'>
                        <select name='status_{$key}' id='status_{$key}' class='form-control form-control-sm task_status' form='admin_table'>
                            <option value='In progress' " . statusInProgress($value[4]) . ">In progress</option>
                            <option value='Complete' " . statusComplete($value[4]) . ">Complete</option>
                        </select>
                    </td>
                    <td class='form-group col-2'>{$value[5]}</td>                    
                </tr>";
        }

        ?>

        </tbody>
    </table>
    <button type='submit' name='submit_edit' class='btn btn-warning' value="Edit form" form='admin_table'>Edit</button>
</form>