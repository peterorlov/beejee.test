<?php

auth();
include_once $_SERVER['DOCUMENT_ROOT'] . "/content/elements/head.php";

?>

<body class="container p-5">
<h3 class="text-center">BeeJee Test</h3>
<div class="row justify-content-center align-items-center mb-2">
        <h1 class="text-muted">Login form</h1>
</div>


<div class="row justify-content-center align-items-center">
    <form action="" method="post" class="col-6 border rounded-lg shadow p-5" name="adminLogin">
        <div class="form-group">
            <label for="exampleInputAdminName">Admin Name</label>
            <input type="text" class="form-control" id="exampleInputAdminName" aria-describedby="AdminName"
                   placeholder="Enter Admin Name" name="admin_name">
        </div>
        <div class="form-group">
            <label for="exampleInputAdminPass">Admin Password</label>
            <input type="password" class="form-control" id="exampleInputAdminPass" aria-describedby="AdminPass"
                   placeholder="Enter Admin Password" name="admin_pass">
        </div>
        <div class="form-group row justify-content-center align-items-center">
            <small id="WarningMessage" class="form-text text-danger"></small>
        </div>
        <button class="btn btn-primary" type="submit" name="admin_login" value="admin_login">Submit Task</button>
    </form>
</div>
<div class="row justify-content-center align-items-center">
    <a href="/" class="btn btn-primary mt-5">Back</a>
</div>

<script src="/scripts/login.js"></script>

<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/content/elements/footer.php";

?>


